# Specification
A bible translation is contained in a so-called module.

A Module is simply a folder that contains various files.

## index.json
For a module to be read, there has to be a index.json file. The file contains basic information about the module, like the name, language, author and license.

It also contains the book names in readable (displayable) format and in the way the directories are named within the module. (E.g. Without special characters and Umlauts)

## Book-Folder
The module contains the the Book in a folder named after it.
In this folder, chapters are seperate sub-folders.

## Chapters
A chapter needs two files: verses.json and headings.json.

### verses.json
The verses.json file contains the text of the verses in an json array.

### headings.json
The headings.json file contains the headings (if there are any) as a json object.

The name of the object is the index of the array in verses.json. (Hint: Arrays start counting from zero.)

So if I wanted to set a heading for Verse 2 and 6, it would look something like this:

```json 
{
    "1": "Test Heading",
    "5": "This story begins..."
}
```